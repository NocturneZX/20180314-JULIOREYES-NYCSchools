//
//  SATScores.swift
//  20180314-JULIOREYES-NYCSchools
//
//  Created by Julio Reyes on 3/16/18.
//  Copyright © 2018 Nocturne Apps. All rights reserved.
//

import Foundation

struct SATScores: Codable {
    var districtNumber: String
    var schoolName: String
    var SATSchoolTakers: String
    var SATCriticalReadingAvgScore: String
    var SATMathAvgScore: String
    var SATWritingAvgScore: String
    
    private enum CodingKeys: String, CodingKey {
        case districtNumber = "dbn"
        case schoolName = "school_name"
        case SATSchoolTakers = "num_of_sat_test_takers"
        case SATCriticalReadingAvgScore = "sat_critical_reading_avg_score"
        case SATMathAvgScore = "sat_math_avg_score"
        case SATWritingAvgScore = "sat_writing_avg_score"
    }
    
    static func endpointForSATScores() -> String{
        return "https://data.cityofnewyork.us/resource/734v-jeq5.json"
    }
    
    enum ResourceError: Error {
        case urlError(reason: String)
        case objectSerialization(reason: String)
    }
}
