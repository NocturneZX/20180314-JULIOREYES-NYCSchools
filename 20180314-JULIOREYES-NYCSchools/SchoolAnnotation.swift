//
//  SchoolAnnotation.swift
//  20180314-JULIOREYES-NYCSchools
//
//  Created by Julio Reyes on 3/16/18.
//  Copyright © 2018 Nocturne Apps. All rights reserved.
//

import Foundation
import MapKit
import Contacts

class SchoolAnnotation: NSObject, MKAnnotation {
    var title: String?
    var schoolLocationAddress: String?
    var borough: String?
    var coordinate: CLLocationCoordinate2D
    

    init(title: String, locationName: String, borough: String, coordinate: CLLocationCoordinate2D) {
        self.title = title
        self.schoolLocationAddress = locationName
        self.borough = borough
        self.coordinate = coordinate
        
        super.init()
    }
    
    func schoolMapItem() -> MKMapItem{
        let schoolDict = [CNPostalAddressStreetKey: subtitle!]
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: schoolDict)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = title
        
        return mapItem
    }
    
    var subtitle: String? {
        return schoolLocationAddress
    }
}
