//
//  SchoolsTableViewController.swift
//  20180314-JULIOREYES-NYCSchools
//
//  Created by Julio Reyes on 3/14/18.
//  Copyright © 2018 Nocturne Apps. All rights reserved.
//

import UIKit

class SchoolsTableViewController: UITableViewController, UISearchBarDelegate {
    
    @IBOutlet weak var schoolSearchBar: UISearchBar!

    var listofSchools = [Schools]()
    var usingSearchFunction: Bool = false
    var filteredListOfSchools = [Schools]()


    override func viewDidLoad() {
        super.viewDidLoad()
        schoolSearchBar.delegate = self
        
        obtainSchoolList { (list) in
            self.listofSchools = list
            self.tableView.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // Safe check to make sure that if the search is being used, return the count of the filtered list else it will use the raw list. However, the ternary function is there if there is no data in the raw list. I have using this method for a long time. It never fails.
        if usingSearchFunction { return self.filteredListOfSchools.count } else { return self.listofSchools.count > 0 ? self.listofSchools.count : 0}
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolCellIdentifier", for: indexPath) as UITableViewCell

        // Configure the cell... If the searhc feature is being used, it'll use the filtered list else the raw list.
        if usingSearchFunction{
            cell.textLabel?.text = self.filteredListOfSchools[indexPath.row].schoolName.capitalized
            cell.detailTextLabel?.text = self.filteredListOfSchools[indexPath.row].schoolNeighborhood?.capitalized
        }else{
            cell.textLabel?.text = self.listofSchools[indexPath.row].schoolName.capitalized
            cell.detailTextLabel?.text = self.listofSchools[indexPath.row].schoolNeighborhood?.capitalized
        }

        return cell
    }
    
    // With the introduction to Swift 4, we are now given more customization to parsing JSON. I decided to use the JSONDecoder for it makes it easier for me to define the properties that the JSON file gives to it. More customization and removes the clutter from the data structures' composition of the NSCoding protocol and traditional JSON parsing (If you parsed JSON between 2012-2015 in Obj-C, you understand). Also, making the data structure Codable would allow me to save the data off-line with the NSKeyedArchiver.
    func obtainSchoolList(complete: @escaping ([Schools]) -> ()){
        let schoolEndpoint = Schools.endpointForSchools()
        
        guard let url = URL(string: schoolEndpoint) else {
            print(Schools.ResourceError.urlError(reason: "Something went wrong. No URL processed. On guard."))
            return
        }
        let urlRequest = URLRequest(url: url)
        
        // Initialize the request for the data task to obtain the list of schools
        let session = URLSession.shared
        let task = session.dataTask(with: urlRequest, completionHandler:{
            (data, response, error) in
            // handle response to request
            // check for error
            guard error == nil else {
                print("Error: did not receive data")
                return
            }
            guard let responseData = data else{
                print("Error: did not receive data")
                return
            }
            
            // Create the JSON decoder
            let decoder = JSONDecoder()
            print(responseData)
            do{
                let schools = try decoder.decode([Schools].self, from: responseData)
                DispatchQueue.main.async {
                    complete(schools)
                }
            } catch{
                print(Schools.ResourceError.urlError(reason: "Something went wrong. Unable to decode. \(error)"))
            }
            
        })
        task.resume()
    }
    
    // I added a search bar feature because it was such a pain scrolling through so many schools
    // MARK: - Search Bar Navigation
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        usingSearchFunction = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        usingSearchFunction = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        usingSearchFunction = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        usingSearchFunction = false;
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        // The filter function is the meat and bones of the search feature. It filters out all the JSON objects into a new set of JSON objects. Back then, you had to use GCD to make sure that the search results came out asynchronously. In Swift, it's jsut easy.

        filteredListOfSchools = listofSchools.filter({( school : Schools) -> Bool in
            return school.schoolName.capitalized.contains(searchText.capitalized)
        })
        if(filteredListOfSchools.count == 0){
            usingSearchFunction = false;
        } else {
            usingSearchFunction = true;
        }
        self.tableView.reloadData()
    }

    
    // MARK: - Navigation
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "showSATScores", sender: self)
    }
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let selectedSchool = segue.destination as? SchoolDetailsSATsViewController{
            selectedSchool.selectedSchool = self.listofSchools[(tableView.indexPathForSelectedRow?.row)!]
        }
    }
    
    
}
