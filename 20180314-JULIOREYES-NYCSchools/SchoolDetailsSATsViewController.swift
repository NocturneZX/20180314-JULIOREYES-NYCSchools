//
//  SchoolDetailsSATsViewController.swift
//  20180314-JULIOREYES-NYCSchools
//
//  Created by Julio Reyes on 3/16/18.
//  Copyright © 2018 Nocturne Apps. All rights reserved.
//

#if (arch(i386) || arch(x86_64)) && os(iOS)
let DEVICE_IS_SIMULATOR = true
#else
let DEVICE_IS_SIMULATOR = false
#endif

import UIKit
import CoreLocation
import MapKit
import MessageUI

class SchoolDetailsSATsViewController: UIViewController,UIScrollViewDelegate,MFMailComposeViewControllerDelegate {
    @IBOutlet weak var scrollView:UIScrollView!
    @IBOutlet weak var mapView:MKMapView!
    
    @IBOutlet weak var emailButton: UIButton!
    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet weak var schoolOverview: UITextView!
    @IBOutlet weak var schoolAddress: UILabel!
    @IBOutlet weak var schoolPhoneNumber: UILabel!
    @IBOutlet weak var schoolEmail: UIButton!
    @IBOutlet weak var schoolTime: UILabel!
    @IBOutlet weak var schoolGraduationRate: UILabel!

    
    @IBOutlet weak var SATSchoolTakersLabel: UILabel!
    @IBOutlet weak var SATCriticalReadingAvgScoreLabel: UILabel!
    @IBOutlet weak var SATMathAvgScoreLabel: UILabel!
    @IBOutlet weak var SATWritingAvgScoreLabel: UILabel!
    @IBOutlet weak var checkSATPrepView:UIView!
    
    var selectedSchool: Schools!
    var schoolSATScores = [SATScores]()
    
    override func viewWillAppear(_ animated: Bool) {
        mapView.delegate = self
        
        // The substring class is deprecated. This is how to get a substring from the start of the string up to an certain index
        if let formattedAddressPosition = selectedSchool.schoolAddress.index(of: "("){
            let format  = selectedSchool.schoolAddress[..<formattedAddressPosition]
            selectedSchool.schoolAddress = String(format) // You have to type cast the string subsequence to a string to finalize the slice
        }
        self.schoolAddress.text = selectedSchool.schoolAddress
        self.schoolName.text = selectedSchool.schoolName
        self.schoolTime.text = "School Time: \(selectedSchool.schoolStartTime ?? "8:00am") to \(selectedSchool.schoolEndTime ?? "5:00pm")"
        self.schoolOverview.text = selectedSchool.schoolOverview
        self.schoolPhoneNumber.text = selectedSchool.schoolPhoneNumber
        
        if let grad = selectedSchool.schoolGraduationRate, let initgradRate = Double(grad){
            var gradRate: Double = (initgradRate * 100)
            gradRate = gradRate.rounded(.toNearestOrEven)
            print(selectedSchool.schoolWebsite!)
            self.schoolGraduationRate.text = "Graduation Rate: \(String(format: "%.2f", gradRate))%"
        }
    
        if selectedSchool.schoolExtraActivities?.range(of: "SAT") != nil  {
            self.checkSATPrepView.isHidden = false
        }

        if let lat = selectedSchool.schoolLatitude, let schoolLatitude = Double(lat) {
            if let longi  = selectedSchool.schoolLongitude, let schoolLongitude = Double(longi) {
                let annotation = SchoolAnnotation(title: selectedSchool.schoolName , locationName: selectedSchool.schoolAddress, borough: selectedSchool.schoolNeighborhood!, coordinate:
                    CLLocationCoordinate2D(latitude: schoolLatitude, longitude: schoolLongitude))
                self.mapView.addAnnotation(annotation)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let searchScoresByDBN = [selectedSchool.districtNumber]
        obtainSchoolSATScores { (list) in
            self.schoolSATScores = list.filter { searchScoresByDBN.contains($0.districtNumber)}
            
            for SATInfo in self.schoolSATScores{
                self.SATSchoolTakersLabel.text = "SAT Applicants: \(String(SATInfo.SATSchoolTakers))"
                self.SATMathAvgScoreLabel.text = "SAT Math Avg. Score: \(String(SATInfo.SATMathAvgScore))"
                self.SATCriticalReadingAvgScoreLabel.text = "SAT Critical Reading Avg. Score:  \(String(SATInfo.SATCriticalReadingAvgScore))"
                self.SATWritingAvgScoreLabel.text = "SAT Writing Avg. Score: \(String(SATInfo.SATCriticalReadingAvgScore))"
            }
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // I honestly wanted to try an Model-View-ViewModel Approach to both the Schools and SATSCORES data types so that the data is fetched as soon as their created instance were initialized but given the moment that I was given the project, my options were limited so good old MVC it is.
    func obtainSchoolSATScores(complete: @escaping ([SATScores]) -> ()){
        let schoolEndpoint = SATScores.endpointForSATScores()
        
        guard let url = URL(string: schoolEndpoint) else {
            print(SATScores.ResourceError.urlError(reason: "Something went wrong. No URL processed."))
            return
        }
        let urlRequest = URLRequest(url: url)
        
        // Initialize the request for the data task to obtain the list of schools
        let session = URLSession.shared
        let task = session.dataTask(with: urlRequest, completionHandler:{
            (data, response, error) in
            // handle response to request
            // check for error
            guard error == nil else {
                print("Error: did not receive data")
                return
            }
            guard let responseData = data else{
                print("Error: did not receive data")
                return
            }
            
            let decoder = JSONDecoder()
            do{
                let scores = try decoder.decode([SATScores].self, from: responseData)
                DispatchQueue.main.async {
                    complete(scores)
                }
            } catch{
                print(Schools.ResourceError.urlError(reason: "Something went wrong. No URL processed. \(error)"))
            }
            
        })
        task.resume()
    }
    
    // MARK: - Navigation
    
    @IBAction func emailButtonPressed() -> (){
        // WARNING: DOES NOT WORK ON SIMULATOR
        if !DEVICE_IS_SIMULATOR{
            let emailTitle = "SAT Inquiry"
            let messageBody = "Can I get more information pertaining to SAT Preparation?"
            let toRecipents = [selectedSchool.schoolEmail]
            let mc: MFMailComposeViewController = MFMailComposeViewController()
            mc.mailComposeDelegate = self
            mc.setSubject(emailTitle)
            mc.setMessageBody(messageBody, isHTML: false)
            mc.setToRecipients(toRecipents as? [String])
            
            self.present(mc, animated: true, completion: nil)
        }
    }
     

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if let schoolWebsite = segue.destination as? WebsiteViewController{
            schoolWebsite.urlString = "https://" + selectedSchool.schoolWebsite!
        }
    }


}

extension SchoolDetailsSATsViewController: MKMapViewDelegate{
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard let annotation = annotation as? SchoolAnnotation else { return nil }
        
        var view: MKMarkerAnnotationView
        
        let identifier = "school"
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            as? MKMarkerAnnotationView {
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            // 5
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            
            let detailLabel = UILabel()
            detailLabel.numberOfLines = 4
            detailLabel.font = detailLabel.font.withSize(8)
            detailLabel.text = annotation.schoolLocationAddress! + " " + annotation.borough!

            view.detailCalloutAccessoryView = detailLabel
            
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5, y: 5)
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
            view.animatesWhenAdded = true
            view.subtitleVisibility = .visible
        }
        
        return view
    }
    
    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
        if let lat = selectedSchool.schoolLatitude, let schoolLatitude = Double(lat) {
            if let longi  = selectedSchool.schoolLongitude, let schoolLongitude = Double(longi) {
                let span = MKCoordinateSpanMake(0.395, 0.395)
                let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: schoolLatitude, longitude: schoolLongitude), span: span)
                mapView.setRegion(region, animated: true)
            }
        }
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
        // With the given location, the Maps application will go and set directions, depending on the user's location
        let location = view.annotation as! SchoolAnnotation
        location.schoolMapItem().openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDefault])
    }
}
