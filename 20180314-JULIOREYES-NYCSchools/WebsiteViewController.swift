//
//  WebsiteViewController.swift
//  20180314-JULIOREYES-NYCSchools
//
//  Created by Julio Reyes on 3/16/18.
//  Copyright © 2018 Nocturne Apps. All rights reserved.
//

import UIKit
import WebKit

class WebsiteViewController: UIViewController, WKNavigationDelegate {

    @IBOutlet var webView:WKWebView!
    var urlString:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        webView.navigationDelegate = self
        let request:URLRequest = URLRequest.init(url: URL.init(string: urlString!)!)
        webView.allowsBackForwardNavigationGestures = true
        self.webView.load(request)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        print("Loading URL: \(String(describing: request.url?.absoluteString))")
        return true;
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func back(sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }

}
