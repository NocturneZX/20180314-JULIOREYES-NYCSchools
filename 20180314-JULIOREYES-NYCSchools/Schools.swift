//
//  Schools.swift
//  20180314-JULIOREYES-NYCSchools
//
//  Created by Julio Reyes on 3/14/18.
//  Copyright © 2018 Nocturne Apps. All rights reserved.
//

import Foundation
import CoreLocation

struct Schools: Codable{
    var districtNumber: String
    var schoolName: String
    var schoolOverview: String
    var schoolGraduationRate: String?
    var schoolAddress: String // location
    var schoolPhoneNumber: String
    var schoolEmail: String?
    var schoolWebsite: String?
    var schoolStartTime: String?
    var schoolEndTime: String?
    var schoolLatitude: String?
    var schoolLongitude: String?
    
    var schoolNeighborhood: String?
    var schoolExtraActivities: String?
    
    
    //It will replace the respective properties name by assigned string
    //while encoding to Json or pList format.
    //Also, while decoding respective Json or pList, these new names are
    //expected in data rather that that defined in class declaration
    
    private enum CodingKeys: String, CodingKey {
        case districtNumber = "dbn"
        case schoolName = "school_name"
        case schoolOverview = "overview_paragraph"
        case schoolAddress = "location"
        case schoolPhoneNumber = "phone_number"
        case schoolEmail = "school_email"
        case schoolWebsite = "website"
        case schoolStartTime = "starttime"
        case schoolEndTime = "end_time"
        case schoolGraduationRate = "graduation_rate"
        case schoolLatitude = "latitude"
        case schoolLongitude = "longitude"
        case schoolNeighborhood = "neighborhood"
        case schoolExtraActivities = "extracurricular_activities"
    }
    
    static func endpointForSchools() -> String{
        return "https://data.cityofnewyork.us/resource/97mf-9njv.json"
    }
    
    enum ResourceError: Error {
        case urlError(reason: String)
        case objectSerialization(reason: String)
    }
    
    
}
